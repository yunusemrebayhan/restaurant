import { Component } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent {
  constructor(private router: Router){}

  foodCategory(category:string):void{
    if (category== 'yemek'){
      this.router.navigate(['/food'])
    }
  }

}
