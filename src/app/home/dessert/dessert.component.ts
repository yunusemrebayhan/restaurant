import { Component } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dessert',
  templateUrl: './dessert.component.html',
  styleUrls: ['./dessert.component.css']
})
export class DessertComponent {
  constructor(private router: Router){}

  DessertCategory(category:string):void{
    if(category== 'tatli'){
      this.router.navigate(['/dessert'])
    }
  }


}
