import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-drink',
  templateUrl: './drink.component.html',
  styleUrls: ['./drink.component.css']
})
export class DrinkComponent {

  constructor(private router:Router){}

  DrinkCategory(category:string):void{
    if(category=='icecek'){
      this.router.navigate(['/drink'])
    }
  }

}
