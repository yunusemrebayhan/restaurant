import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/signup/signup.component'
import { HomeComponent } from './home/home.component';
import { FoodComponent } from './home/food/food.component';
import { DessertComponent } from './home/dessert/dessert.component';
import { DrinkComponent } from './home/drink/drink.component';
import { AboutUsComponent } from './about-us/about-us.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  {path:'home', component: HomeComponent},
  {path:'food', component: FoodComponent},
  {path:'dessert',component:DessertComponent},
  {path:'drink',component: DrinkComponent},
  {path:'aboutUs',component:AboutUsComponent}
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }