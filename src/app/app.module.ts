import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloComponent } from './hello.component';
import { UserService } from './service/user.service';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './auth/login/login.component';
import { SignUpComponent } from './auth/signup/signup.component';
import { HomeComponent } from './home/home.component';
import { FoodComponent } from './home/food/food.component';
import { DessertComponent } from './home/dessert/dessert.component';
import { DrinkComponent } from './home/drink/drink.component';
import { AboutUsComponent } from './about-us/about-us.component';


@NgModule({
  imports:      [ BrowserModule, FormsModule ,AppRoutingModule ],
  declarations: [ AppComponent, HelloComponent, LoginComponent, SignUpComponent, HomeComponent, FoodComponent, DessertComponent, DrinkComponent, AboutUsComponent ],
  bootstrap:    [ AppComponent ],
  providers: [UserService]
})
export class AppModule { }
