import { Component, OnInit, AfterViewInit, ViewEncapsulation, Renderer2 } from '@angular/core';
import { UserService } from './../../service/user.service';
import Keyboard from 'simple-keyboard';

@Component({
  selector: 'app-sign-up',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit, AfterViewInit {
  userData: any; // Uygun tipi kullanmalısınız
  keyboard!: Keyboard;
  value = '';
  selectedAllergies: string;
  allergiesList = [{allergies: 'milk'}, {allergies: 'eggs'}, {allergies: 'peanuts'},  {allergies: 'peas'},  {allergies: 'nuts'} , {allergies: 'meat'},  {allergies: 'fish'}];
  i: number = 0;

  constructor(private user: UserService, private renderer: Renderer2) { 
    this.selectedAllergies = this.allergiesList[0].allergies;
  }

  ngOnInit() {
    this.user.currentUserData.subscribe(userData => this.userData = userData);
  }

  signUp(data:any) {
    this.user.changeData(data);
  }

  ngAfterViewInit() {
    this.keyboard = new Keyboard({
      onChange: (input) => this.onChange(input),
      onKeyPress: (button) => this.onKeyPress(button),
      mergeDisplay: true,
      layoutName: 'default',
      layout: {
        default: [
          'q w e r t y u i o p',
          'a s d f g h j k l',
          '{shift} z x c v b n m {backspace}',
          '{numbers} {space} {ent}'
        ],
        shift: [
          'Q W E R T Y U I O P',
          'A S D F G H J K L',
          '{shift} Z X C V B N M {backspace}',
          '{numbers} {space} {ent}'
        ],
        numbers: ['1 2 3', '4 5 6', '7 8 9', '{abc} 0 {backspace}']
      },
      display: {
        '{numbers}': '123',
        '{ent}': 'return',
        '{escape}': 'esc ⎋',
        '{tab}': 'tab ⇥',
        '{backspace}': '⌫',
        '{capslock}': 'caps lock ⇪',
        '{shift}': '⇧',
        '{controlleft}': 'ctrl ⌃',
        '{controlright}': 'ctrl ⌃',
        '{altleft}': 'alt ⌥',
        '{altright}': 'alt ⌥',
        '{metaleft}': 'cmd ⌘',
        '{metaright}': 'cmd ⌘',
        '{abc}': 'ABC'
      }
    });

    // Form elemanına ngModel bağlamak yerine Reactive Forms kullanımı
    this.keyboard.setInput(this.value);
    this.renderer.setProperty(this.keyboard.input, 'value', this.value);
  }

  onChange = (input: string) => {
    this.value = input;
    console.log('Input changed', input);
  };

  onKeyPress = (button: string) => {
    console.log('Button pressed', button);

    if (button === '{shift}' || button === '{lock}') this.handleShift();
  };

  handleShift = () => {
    const currentLayout = this.keyboard.options.layoutName;
    const shiftToggle = currentLayout === 'default' ? 'shift' : 'default';

    this.keyboard.setOptions({
      layoutName: shiftToggle
    });
  };

  handleNumbers() {
    const currentLayout = this.keyboard.options.layoutName;
    const numbersToggle = currentLayout !== 'numbers' ? 'numbers' : 'default';

    this.keyboard.setOptions({
      layoutName: numbersToggle
    });
  }

  keyPress(event:any): void {
    this.i = this.allergiesList.map(v => v.allergies == this.selectedAllergies).indexOf(true);
       if (event.keyCode === 120) {
         this.i++;
         if (this.i < this.allergiesList.length) {
           this.selectedAllergies = this.allergiesList[this.i].allergies;
         } else {
           this.selectedAllergies = this.allergiesList[0].allergies;
           this.i = 0;
         }    
        }
     } 

}
